/** \file StructureContainer.java */

package com.arnaud.metronome;

import android.content.Context;

import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;

/** The Basic elements with its own byte[] for signal storage */
class Base implements Serializable {
	double bpm;
	double bar;
	
	byte[] tic;
	byte[] toc;
	boolean isSetup = false; /**< A base is set up when its signal as been generated, no needs to re-generated it after */
}

/** A Pattern are composed of several Base */
class Pattern implements Serializable {
	/** Small encapsulated class telling wich base to use (index) for n-times (times) */
	static class Base implements Serializable {
		int index;
		int times;
	}
	
	Pattern.Base[] bases;
	boolean isSetup = false; /**< A pattern is set up when every bases it points to are already set up */
}

/** A Structure is composed of several Pattern */
class Structure implements Serializable {
	/** Small encapsulated class telling wich pattern to use (index) for n-times (times) */
	static class Pattern implements Serializable {
		int index;
		int times;
	}
	
	Structure.Pattern[] patterns;
}

/** Commodity class storing structure data, storing signals during clicking and processing IO with phone.
* It is serializable for passing it through intent and files
*/
class StructureContainer implements Serializable {
	private String FILENAME = "structures";
	
	Structure[] structures = new Structure[0];
	Pattern[] patterns = new Pattern[0];
	Base[] bases = new Base[0];
	
	/** All the Things You Are version Gerald Clayton */
	public void initAllThings ()
	{
		bases = new Base[3];
		for (int i=0; i<3 ; i++)
			bases[i] = new Base();
		bases[0].bpm = 120;
		bases[0].bar = 2;
		bases[1].bpm = 160;
		bases[1].bar = 2;
		bases[2].bpm = 120;
		bases[2].bar = 1;
		
		patterns = new Pattern[3];
		for (int i=0; i<3 ; i++)
			patterns[i] = new Pattern();
			
		patterns[0].bases = new Pattern.Base[2];
			patterns[0].bases[0] = new Pattern.Base();
			patterns[0].bases[1] = new Pattern.Base();
			patterns[0].bases[0].index = 0;
			patterns[0].bases[0].times = 1;
			patterns[0].bases[1].index = 1;
			patterns[0].bases[1].times = 1;
		patterns[1].bases = new Pattern.Base[1];
			patterns[1].bases[0] = new Pattern.Base();
			patterns[1].bases[0].index = 1;
			patterns[1].bases[0].times = 2;
		patterns[2].bases = new Pattern.Base[2];
			patterns[2].bases[0] = new Pattern.Base();
			patterns[2].bases[1] = new Pattern.Base();
			patterns[2].bases[0].index = 1;
			patterns[2].bases[0].times = 1;
			patterns[2].bases[1].index = 2;
			patterns[2].bases[1].times = 1;
		
		structures = new Structure[1];
		structures[0] = new Structure();
		structures[0].patterns = new Structure.Pattern[3];
		for (int i=0; i<3; i++)
			structures[0].patterns[i] = new Structure.Pattern();
			
			structures[0].patterns[0].index = 0;
			structures[0].patterns[0].times = 8;
			structures[0].patterns[1].index = 1;
			structures[0].patterns[1].times = 4;
			structures[0].patterns[2].index = 2;
			structures[0].patterns[2].times = 6;
	}
	
	/** Clave 12/8 de base */
	public void initEwe ()
	{
		bases = new Base[2];
		bases[0] = new Base();
		bases[1] = new Base();
		bases[0].bpm = 180;
		bases[0].bar = 1;
		bases[1].bpm = 180;
		bases[1].bar = 0.5;
		
		patterns = new Pattern[1];
		patterns[0] = new Pattern();
			
		patterns[0].bases = new Pattern.Base[4];
		for (int i=0; i<4; i++)
			patterns[0].bases[i] = new Pattern.Base();
			
			patterns[0].bases[0].index = 0;
			patterns[0].bases[0].times = 2;
			patterns[0].bases[1].index = 1;
			patterns[0].bases[1].times = 1;
			patterns[0].bases[2].index = 0;
			patterns[0].bases[2].times = 3;
			patterns[0].bases[3].index = 1;
			patterns[0].bases[3].times = 1;
		
		structures = new Structure[1];
		structures[0] = new Structure();
		structures[0].patterns = new Structure.Pattern[1];
			structures[0].patterns[0] = new Structure.Pattern();
			structures[0].patterns[0].index = 0;
			structures[0].patterns[0].times = 1;
	}
	
	/** Clave latin 2-3 de base */
	public void init23 ()
	{
		bases = new Base[4];
		for (int i=0; i<4 ;i++)
			bases[i] = new Base();
		bases[0].bpm = 120;
		bases[0].bar = 1;
		bases[1].bpm = 120;
		bases[1].bar = 0.75;
		bases[2].bpm = 120;
		bases[2].bar = 0.5;
		bases[3].bpm = 240;
		bases[3].bar = 0;
		
		patterns = new Pattern[1];
		patterns[0] = new Pattern();
			
		patterns[0].bases = new Pattern.Base[5];
			for (int i=0; i<5; i++)
				patterns[0].bases[i] = new Pattern.Base();
			patterns[0].bases[0].index = 3;
			patterns[0].bases[0].times = 1;
			patterns[0].bases[1].index = 2;
			patterns[0].bases[1].times = 1;
			patterns[0].bases[2].index = 0;
			patterns[0].bases[2].times = 1;
			patterns[0].bases[3].index = 1;
			patterns[0].bases[3].times = 2;
			patterns[0].bases[4].index = 2;
			patterns[0].bases[4].times = 1;
		
		structures = new Structure[1];
		structures[0] = new Structure();
		structures[0].patterns = new Structure.Pattern[1];
			structures[0].patterns[0] = new Structure.Pattern();
			structures[0].patterns[0].index = 0;
			structures[0].patterns[0].times = 1;
	}

	/** Internal memory means not sdcard and no permissions headaches needed */
	public void saveToInternal (Context context)
	{
		try {//Forcé de faire ça. Java c'est reloud.
			FileOutputStream fos = context.openFileOutput(FILENAME, context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public StructureContainer loadFromInternal (Context context)
	{
		StructureContainer container = new StructureContainer();
		
		try {
			FileInputStream fis = context.openFileInput(FILENAME);
			ObjectInputStream ois = new ObjectInputStream(fis);
			container = (StructureContainer) ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return container;
	}
	
	/** Should be called each time ClickerStructure is stopped.
	* If samplerate, bpm or clicsize is changed then the signals need to be re-generated
	*/
	public void resetAudio() {
		for(int i=0;i<bases.length;i++) {
			bases[i].isSetup = false;
			bases[i].tic = null;
			bases[i].toc = null;
		}
		for(int i=0;i<patterns.length;i++)
			patterns[i].isSetup = false;
	}
}
